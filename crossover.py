# CROSSOVER ####################################################################
from constants import CROSSOVER, CROSSOVER_MULTIPOINT_NUMBER

import random

def crossover (args):
	if CROSSOVER == 'onepoint':
		return crossover_onepoint (args)
	elif CROSSOVER == 'multipoint':
		return crossover_multipoint (args)
	elif CROSSOVER == 'uniform':
		return crossover_uniform (args)
	elif CROSSOVER == 'defined':
		from user_defined import crossover_defined
		return crossover_defined (args)
	else:
		raise Exception('Crossover method unknown.')

def crossover_onepoint ((parentA, parentB)):
	childA = [[]] * len(parentA)
	childB = [[]] * len(parentA)
	cross_point = random.randint(0, len(parentA)-1)

	for gene in range(len(parentA)):
		childA[gene] = parentA[gene] if gene < cross_point else parentB[gene]
		childB[gene] = parentB[gene] if gene < cross_point else parentA[gene]

	return (childA, childB)

def crossover_multipoint ((parentA, parentB)):
	childA = [[]] * len(parentA)
	childB = [[]] * len(parentA)
	cross_points = []
	points_num = (CROSSOVER_MULTIPOINT_NUMBER if CROSSOVER_MULTIPOINT_NUMBER <= (len(parentA)-2) else len(parentA) - 2)
	parentmap = []
	parent = (True if random.random() < 0.5 else False)

	while len(cross_points) < points_num:
		p = random.randint(1, len(parentA) - 2)

		if p not in cross_points:
			cross_points.append(p)

	for x in range(points_num):
		pos = cross_points.pop(cross_points.index(min(cross_points)))
		for i in range(len(parentmap), pos):
			parentmap.append(parent)
		parent = not parent

	for pos in range(len(parentA) - len(parentmap)):
		parentmap.append(parent)

	for gene in range(len(parentA)):
		childA[gene] = parentA[gene] if parentmap[gene] else parentB[gene]
		childB[gene] = parentB[gene] if not parentmap[gene] else parentA[gene]

	return (childA, childB)

def crossover_uniform ((parentA, parentB)):
	childA = [[]] * len(parentA)
	childB = [[]] * len(parentA)

	for gene in range(len(parentA)):
		rnd = random.random()
		childA[gene] = parentA[gene] if rnd < 0.5 else parentB[gene]
		childB[gene] = parentB[gene] if rnd < 0.5 else parentA[gene]

	return (childA, childB)
