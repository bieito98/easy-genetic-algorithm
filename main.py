import random
import time

from constants import *
from init import init_population
from fitness import fitness, population_fitness
from selection import selection
from crossover import crossover
from mutation import mutation
from io_tools import print_options, print_header, print_data, print_best, file_exists, load_json, save_json
from user_defined import init_individual


def genetic_algorithm ():
	random.seed(time.time())

	print_options()
	print_header()

	best = {'individual' : [], 'fitness' : 0, 'generation' : 0}
	no_imp_gens = 0
	gen = 1

	# create initial population
	population = init_population()

	while gen <= MAX_GENERATIONS and no_imp_gens < MAX_NO_IMPROVEMENTS_GENS:
		try:
			new_population = []
			ordered_population = []
			fitnesses = []
			ordered_fitnesses = []

			# get fitnesses and order by them
			fitnesses = population_fitness (population)
			ordered_fitnesses = list(fitnesses)
			ordered_fitnesses.sort()

			# order population by fitness
			for indiv in range(POPULATION_SIZE):
				ordered_population.append(population[get_real_index(indiv, fitnesses, ordered_fitnesses)])

			# select for crossover
			parents = selection(population, fitnesses)

			# create new population
			while len(new_population) < POPULATION_SIZE:
				if random.random() < CROSSOVER_RATIO:
					children = crossover(parents[random.randint(0, len(parents)-1)])
					new_population.append(best_child(children))
				else:
					new_population.append(population[random.randint(0, POPULATION_SIZE-1)])

			# perform mutations
			for indiv in new_population:
				indiv = mutation(indiv)

			# save best
			if ordered_fitnesses[0] > best['fitness']:
				best['fitness'] = ordered_fitnesses[0]
				best['individual'] = ordered_population[0]
				best['generation'] = gen
				no_imp_gens = 0
			else:
				no_imp_gens += 1

			# new_population is population for next iteration
			population = list(new_population)
			gen += 1

			print_data(gen-1, ordered_fitnesses, no_imp_gens, best['fitness'])

		except KeyboardInterrupt:
			print
			inpt = ''
			accepted = ['resume', 'stop']
			while inpt not in accepted:
				inpt = raw_input('Paused. Enter "inject_diversity", "options", "resume", "save_best", "save_population", "show" or "stop"\n')
				print
				if inpt == 'show':
					print 'Current GENERATION: ' + str(gen)
					print_best(best['generation'], int(best['fitness']), best['individual'])
				elif inpt == 'save_best':
					save_value([best['individual']])
				elif inpt == 'save_population':
					save_value(population)
				elif inpt == 'inject_diversity':
					if inject_diversity(population) and RSET_ON_DIV_INJ:
						no_imp_gens = 0
				elif inpt == 'options':
					print_options()
			if inpt == 'resume':
				print
				print_header()
				continue
			elif inpt == 'stop':
				break

	print
	print 'TOTAL NUMBER OF GENERATIONS: ' + str(gen-1)
	print_best(best['generation'], int(best['fitness']), best['individual'])

	while True:
		answ = raw_input('Save last generation or best individual? (gen/best/exit)\n')
		print
		if answ == 'best':
			save_value([best['individual']])
			print('Last generation saved\n')
		elif answ == 'gen':
			save_value(population)
			print('Best individual saved\n')
		elif answ == 'exit':
			break

# OTHER FUNCTIONS ##############################################################
def best_child ((child_a, child_b)):
	return (child_a if fitness(child_a) > fitness(child_b) else child_b)

def get_real_index (ordered_index, unordered_list, ordered_list):
	return unordered_list.index(ordered_list[ordered_index])

def save_value (value):
	path = raw_input('Enter path of file to store data in:\n')
	print

	if file_exists (path):
		while True:
			cond = raw_input('"' + path + '" already exists. Do you want to overwrite it? (y/n)\n')
			print
			if cond == 'y':
				save_json(value, path)
				break
			elif cond == 'n':
				print('Cancelled\n')
				break
	else:
		save_json(value, path)

def inject_diversity (population):
	new_pop = []
	#check input method and paths
	while True:
		method = raw_input('Enter addition method: (random/file)\n')
		print
		if method == 'random':
			indiv = ' random individuals'
			break
		elif method == 'file':
			while True:
				path = raw_input('Enter path of file to load data from:\n')
				print
				if file_exists(path):
					new_pop = load_json(path)
					indiv = ' individuals from file '
					break
				else:
					print('File not found\n')
			break
	# check number of new individuals
	size = input('Enter number of individuals to be added:\n')
	print
	if size <= 0:
		print('ERROR. At least 1 individual must be added\n')
		return False
	if size > POPULATION_SIZE:
		print('ERROR. Maximum size for population is ' + str(POPULATION_SIZE) + '\n')
		return False
	if method == 'file' and size > len(new_pop):
		print('ERROR. Not enough individuals in file "' + path + '"\n')
		return False

	while True:
		cond = raw_input('Inject ' + str(size) + indiv + ('"' + path + '"' if method == 'file' else '') + '? (y/n)\n')
		print
		if cond == 'y':
			for ind in range(size):
				population.pop()

			if method == 'random':
				new_pop.append(init_individual())
			if method == 'file':
				for ind in range(len(new_pop) - size):
					new_pop.pop()

			population.extend(new_pop)
			return True
		elif cond == 'n':
			print('Cancelled\n')
			return False
