# INIT #########################################################################
from constants import POPULATION_SIZE
from user_defined import init_individual

def init_population ():
	population = []
	
	for i in range(POPULATION_SIZE):
		population.append(init_individual())

	return population
