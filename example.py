import random
LENGTH = 10
''' EXAMPLE:
Find maximum of function f(x) = - x*2**9 + x*2**8 - x*2**7 + x*2**6 - x*2**5 + x*2**4 - x*2**3 + x*2**2 - x*2 + x,
where x belongs to [-1, 0, 1]
Solutions are codified as lists of 10 numbers, one for each exponent

'''
# Init
def init_individual ():
	'''
	Creates a single individual that represents a candidate solution to the
	problem that has to be solved.
	The format of the individual should be a list of genes.
	This function is used to create the initial population for the algorithm.
	'''
	new_individual = [0]*LENGTH

	for i in range(LENGTH):
		p = random.random()
		if p < 1.0/3.0:
			new_individual[i] = -1
		elif p > 2.0/3.0:
			new_individual[i] = +1
		else:
			new_individual[i] = 0
	#raise Exception('Init function not implemented. See user_defined.init_individual')

	return new_individual

# Fitness
def fitness_function (individual):
	'''
	This function is used to numerically determine the goodness of an
	individual as a	solution to the problem.
	'''
	fitness = 0

	#raise Exception('Fitness function not implemented. See user_defined.fitness_function')
	for i in range(len(individual)):
		if i % 2 == 0:
			fitness += individual[i]*2**i
		else:
			fitness -= individual[i]*2**i

	return fitness

# Mutation
def mutate_gene (gene):
	'''
	This function is used to determine the way a gene from an individual is
	mutated.
	'''
	mutated_gene = (-1*gene if gene != 0 else -1)

	#raise Exception('Gen mutation function not implemented. See user_defined.mutate_gene')
	
	return mutated_gene
