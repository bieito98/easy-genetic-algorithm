# IO TOOLS #####################################################################
from constants import *
import json

def print_best (gen, fitness, individual):
	print 'Best individual found at GEN No. ' + str(gen) + ' with FITNESS: ' + str(fitness)
	print

def print_options ():
	print 'OPTIONS: '
	print '  POPULATION SIZE    ' + str(POPULATION_SIZE)
	print '  MAX GENERATIONS    ' + str(MAX_GENERATIONS)
	print '  MAX NO IMPRV GEN   ' + str(MAX_NO_IMPROVEMENTS_GENS)
	print '  CROSSOVER TYPE     ' + str(CROSSOVER)
	print '  CROSSOVER RATIO    ' + str(CROSSOVER_RATIO)
	print '  MUTATION TYPE      ' + str(MUTATION)
	print '  MUTATION RATIO     ' + str(MUTATION_RATIO)
	print '  PAIRS OF PARENTS   ' + str(PAIRS_OF_PARENTS)
	print '  ELITE SIZE         ' + str(ELITE_SIZE)
	print '  TOURNAMENT SIZE    ' + str(TOURNAMENT_SIZE)
	print '  RESET ON DIV INJ   ' + str(RSET_ON_DIV_INJ)
	print

def print_header ():
	if VERBOSE:
		print '  GENERATION    FITNESS    BEST FITNESS    UNIMPROVED  GENS'
		print

def print_data (gen, ordered_fitnesses, no_imp_gens, best_fitness):
	if VERBOSE:
		print (' '*(len('GENERATION') + 1 - len(str(gen))) + str(gen)
			+ ' '*(4 + len('FITNESS') - len(str(int(ordered_fitnesses[0])))) + str(int(ordered_fitnesses[0]))
			+ ' '*(4 + len('BEST FITNESS') - len(str(int(best_fitness)))) + str(int(best_fitness))
			+ ' '*(4) + '[' + '+'*(no_imp_gens*16/MAX_NO_IMPROVEMENTS_GENS) + ' '*(16-no_imp_gens*16/MAX_NO_IMPROVEMENTS_GENS) + ']'
			+ ' '*(len(str(MAX_NO_IMPROVEMENTS_GENS)) - len(str(no_imp_gens)) + 2) + str(no_imp_gens) + '/' + str(MAX_NO_IMPROVEMENTS_GENS))

def load_json(path):
	with open(path, 'r') as file:
		return json.load(file)

def save_json(var, path):
	with open(path, 'w') as file:
		json.dump(var, file)
		
def file_exists (path):
	res = True
	try:
		fd = open(path, 'r')
		fd.close()
	except IOError:
		res = False
	return res

