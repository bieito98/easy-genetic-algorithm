POPULATION_SIZE = 64
MAX_GENERATIONS = 100
MAX_NO_IMPROVEMENTS_GENS = 1000
RSET_ON_DIV_INJ = True  # resets no improvement gens counter on diversity injection

# onepoint, multipoint, uniform, defined (by user)
CROSSOVER = 'uniform'
CROSSOVER_MULTIPOINT_NUMBER = 3
CROSSOVER_RATIO = 0.9	# ~0.9

# onegene, multigene, defined (by user)
MUTATION = 'multigene'
MUTATION_RATIO = 0.1	# 0.05-0.10

PAIRS_OF_PARENTS = 32
ELITE_SIZE = 0

# tournament, defined (by user)
SELECTION = 'tournament'
TOURNAMENT_SIZE = 8

VERBOSE = True
