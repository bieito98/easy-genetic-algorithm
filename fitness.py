# FITNESS ######################################################################
from user_defined import fitness_function

def fitness (individual):
	return fitness_function (individual)

def population_fitness (population):
	return [fitness(individual) for individual in population]
