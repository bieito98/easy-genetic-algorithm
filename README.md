# easy-genetic-algorithm

A modular genetic algorithm designed in a simple way in order to make it easy to understand how it works. Despite its academic purposes, it can also be easily modified to be used in a real situation.