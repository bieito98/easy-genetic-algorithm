# MUTATION #####################################################################
from constants import MUTATION, MUTATION_RATIO
from user_defined import mutate_gene
import random

def mutation (args):
	if MUTATION == 'onegene':
		return mutation_onegene(args)
	elif MUTATION == 'multigene':
		return mutation_multigene(args)
	elif MUTATION == 'defined':
		from user_defined import mutation_defined
		return mutation_defined (args)
	else:
		raise Exception('Mutation method unknown.')

def mutation_onegene (individual):
	if random.random() < MUTATION_RATIO:
		gene = random.randint(0, len(individual) - 1)
		individual[gene] = mutate_gene(individual[gene])

	return individual

def mutation_multigene (individual):
	for gene in range(len(individual)):
		if random.random() * len(individual) < MUTATION_RATIO:
			individual[gene] = mutate_gene(individual[gene])

	return individual
