# SELECTION ####################################################################
from constants import PAIRS_OF_PARENTS, POPULATION_SIZE, SELECTION, TOURNAMENT_SIZE

import random
# TODO: implement roulette selection
def selection (population, fitnesses):
	return selection_tournament (population, fitnesses)
	if SELECTION == 'tournament':
		return selection_tournament (population, fitnesses)
	elif SELECTION == 'defined':
		return selection_defined (population, fitnesses)
	elif SELECTION == 'random':
		from user_defined import selection_defined
		return selection_random (population, fitnesses)
	else:
		raise Exception('Selection method unknown.')

def selection_random (population, fitnesses):
	pair_list = []
	for p in range(PAIRS_OF_PARENTS):
		fst = random.randint(0, POPULATION_SIZE-1)
		snd = random.randint(0, POPULATION_SIZE-1)
		while fst == snd:
			snd = random.randint(0, POPULATION_SIZE-1)
		pair = (population[fst], population[snd])
		pair_list.append(pair)

	return pair_list

# TODO: use fitnesses instead of the order of individuals
def do_tournament (fitnesses):
	players = []
	for n in range(TOURNAMENT_SIZE):
		players.append(random.randint(0, POPULATION_SIZE-1))
	return min(players)

def selection_tournament (population, fitnesses):
	pair_list = []
	for p in range(PAIRS_OF_PARENTS):
		fst = do_tournament(fitnesses)
		snd = do_tournament(fitnesses)
		while fst == snd:
			snd = do_tournament(fitnesses)
		pair = (population[fst], population[snd])
		pair_list.append(pair)
	return pair_list
