# Init
def init_individual ():
	'''
	Creates a single individual that represents a candidate solution to the
	problem that has to be solved.
	The format of the individual should be a list of genes.
	This function is used to create the initial population for the algorithm.
	'''
	new_individual = [0]*10

	raise Exception('Init function not implemented. See user_defined.init_individual')

	return new_individual

# Fitness
def fitness_function (individual):
	'''
	This function is used to numerically determine the goodness of an
	individual as a	solution to the problem.
	'''
	fitness = 0

	raise Exception('Fitness function not implemented. See user_defined.fitness_function')

	return fitness

# Selection
def selection_defined (population, fitnessess):
	'''
	This function is used to select the individuals that will be used as parents
	to create the individuals for the next generation.
	This function needs not to be implemented, except none of the default is
	valid.
	'''
	list_of_selected_parents = [(0,1), (0,1)]

	raise Exception('Selection function not implemented.')

	return list_of_selected_parents

# Crossover
def crossover_defined ((parentA, parentB)):
	'''
	This function is used to create a pair of new individuals from a pair of
	parents (individuals from the current generation).
	This function needs not to be implemented, except none of the default is
	valid.
	'''
	childA = parentA
	childB = parentB

	raise Exception('Crossover function not implemented.')

	return (childA, childB)

# Mutation
def mutation_defined (individual):
	'''
	This function is used to mutate (or not) the genes of the given individual.
	This function needs not to be implemented, except none of the default is
	valid.
	'''
	mutated_individual = individual

	raise Exception('Mutation function not implemented.')

	return mutated_individual

def mutate_gene (gene):
	'''
	This function is used to determine the way a gene from an individual is
	mutated.
	'''
	mutated_gene = gene

	raise Exception('Gen mutation function not implemented. See user_defined.mutate_gene')

	return mutated_gene
